package com.kodframe.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class AuthApplication {

	@RequestMapping("/")
    String hello(){
	    return "hewllo world";
    }

	public static void main(String[] args) {
		SpringApplication.run(AuthApplication.class, args);
	}
}
